from flask import Flask, jsonify, request

from flask_swagger_ui import get_swaggerui_blueprint

from models.ModelKPI import response
from AgileDevelopmentKPI import members, agile_kpi_schema

app = Flask(__name__)

# swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Seans-Python-Flask-REST-Boilerplate"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)


# end swagger specific ###

@app.route('/')
def index():
    return jsonify(response)


@app.route('/agileDevelopment', methods=['GET', 'POST'])
def agile_development():
    if request.method == 'GET':
        return {
            "Response": 200,
            "Data": agile_kpi_schema.dump(members)
        }
    if request.method == 'POST':
        return {
            "Response": 200,
            "Data": agile_kpi_schema.dump(members)
        }


@app.route('/agileDevelopment3/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def agile_development_for_id(id):
    if request.method == 'GET':
        return {
            "Response": 200,
            "Data": id
        }

    if request.method == 'PUT':
        return {
            "Response": 200,
            "Data": id
        }

    if request.method == 'DELETE':
        return {
            "Response": 200,
            "Data": id
        }


if __name__ == '__main__':
    app.run(port=3000, debug=True)
