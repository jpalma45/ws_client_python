from marshmallow import Schema


class AgileDevelopmentKPI(object):

    # Initialize the attributes
    def __init__(self, member, member_happiness=None, member_productivity=None,
                 milestone_integration=None, milestone_demo_progress=None,
                 milestone_tracking_service=None, recent_activity=None,
                 task_complete=None, task_complete_this_month=None,
                 task_complete_last_month=None, task_complete_all_project=None):
        self.member = member
        self.member_happiness = member_happiness
        self.member_productivity = member_productivity
        self.milestone_integration = milestone_integration
        self.milestone_demo_progress = milestone_demo_progress
        self.milestone_tracking_service = milestone_tracking_service
        self.recent_activity = recent_activity
        self.task_complete = task_complete
        self.task_complete_this_month = task_complete_this_month
        self.task_complete_last_month = task_complete_last_month
        self.task_complete_all_project = task_complete_all_project

    def __str__(self):
        return str(self.member) + ' ' + str(self.member_happiness) + ' ' + str(self.member_productivity) + ' ' \
               + str(self.task_complete_this_month) + ' ' + str(self.task_complete_last_month) + ' ' + str(
            self.task_complete_all_project)


class AgileDevelopmentKPISchema(Schema):
    class Meta:
        fields = ('member', 'member_productivity',
                  'task_complete_this_month', 'task_complete_last_month',
                  'task_complete_all_project')


agile_kpi_schema = AgileDevelopmentKPISchema()

members = AgileDevelopmentKPI(123)
members.member_happiness = 312
members.member_productivity = 234
members.task_complete_this_month = 300
members.task_complete_last_month = 200
members.task_complete_all_project = 250
print(members)
